import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/app.component';
import PostsIndex from './components/posts_index.component';
import PostsNew from './components/posts_new.component';
import PostShow from './components/posts_show.component';

export default (
  <Route path="/" component={App} >
    <IndexRoute component={PostsIndex} />
    <Route path="posts/new" component={PostsNew} />
    <Route path="posts/:id" component={PostShow} />
  </Route>
);