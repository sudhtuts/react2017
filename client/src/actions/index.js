import axios from 'axios';

export const FETCH_POSTS = 'FETCH_POSTS';
export const FETCH_POST = 'FETCH_POST';
export const DELETE_POST = 'DELETE_POST';
export const CREATE_POST = 'CREATE_POST';
const API_URL = 'https://react-posts.herokuapp.com/';

export function fetchPosts() {
  
  const request = axios.get(`${API_URL}/posts.json`);

  return {
    type: FETCH_POSTS,
    payload: request
  };
}

export function fetchPost(id) {
  const request = axios.get(`${API_URL}/posts/${id}.json`);

  return {
    type: FETCH_POST,
    payload: request
  };
}

export function deletePost(id) {
  const request = axios.delete(`${API_URL}/posts/${id}.json`);

  return {
    type: DELETE_POST,
    payload: request
  };
}

export function createPost(props) {
  const request = axios.post(`${API_URL}/posts.json`, props);

  return {
    type: 'CREATE_POST',
    payload: request
  };
}